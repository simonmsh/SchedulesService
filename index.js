var http = require('http');
var fs = require('fs');
var path = require('path');


var server = http.createServer(function(request, response) {

    //response.writeHead(200, {"Content-Type": "text/plain"});
    //response.write("t2<br/>\n");

    var filePath = request.url;
    var contentType = "application/json";
    filePath = "\data.json";
    //response.write("filePath=" + filePath + "\n");


    //if (fs) response.write("fs exists\n");   
    fs.readFile(filePath, function(error, content) {
        if (error) {
            response.write("marker6\n");            
            if(error.code == 'ENOENT'){
                response.write("marker3=" + filePath + "\n");                
                //fs.readFile('./404.html', function(error, content) {
                //    response.writeHead(200, { 'Content-Type': contentType });
                    response.end(content, 'utf-8');
                //});
            }
            else {
                response.writeHead(500);
                response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
                response.end(); 
            }
        }
        else {
            response.writeHead(200, { 'Content-Type': contentType });
            //response.write("marker7=" + filePath + "\n");

            response.end(content, 'utf-8');
        }
    });

    //response.end("Hello World!");

});

var port = process.env.PORT || 8000;
server.listen(port);

console.log("Server running at http://localhost:%d", port);
